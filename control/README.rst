=============================================
 README for Adélie Linux Site Control System
=============================================
:Authors:
  * **A. Wilcox**, author
  * **Adélie Linux Developers and Users**, contributions
:Status:
  Production
:Copyright:
  © 2018 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux Site Control System.  This software
is used to control the Adélie Linux Web server.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  For security-sensitive updates, contact the Security Team at
sec-bugs@adelielinux.org.




Contents
========

``githook.py``: GitLab hook listener
````````````````````````````````````
``githook.py`` listens for GitLab push events, and then manipulates the file
system to trigger ``site-control`` to perform the needed updates.


``main.cc``: Implementation for ``site-control``
````````````````````````````````````````````````
``site-control`` watches the file system to determine when to perform updates.
When triggered by ``githook.py``, ``site-control`` will pull changes from the
changed Git repository, and then (if necessary) rebuild the files.




Usage
=====

Run ``qmake`` (or ``qt5-qmake``) to generate a ``Makefile`` for
``site-control``.  Then run ``make``.

``githook.py`` is typically run by a uWSGI process; the ini file is contained
in this directory as ``githook.ini``.
